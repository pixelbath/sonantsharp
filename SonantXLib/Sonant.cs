﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SonantXLib
{
    public class Sonant
    {
        private const int samplesPerSecond = 44100;
        private const int channels = 2;
        private const int maxTimeMS = 33;

        private double oscSin(double input)
        {
            return Math.Sin(input * 6.283184);
        }

        private double oscSquare(double input)
        {
            if (oscSin(input) < 0) return -1;
            return 1;
        }

        private double oscSaw(double input)
        {
            return (input % 1) - 0.5;
        }

        private double oscTri(double input)
        {
            double temp = (input % 1) * 4;
            if (temp < 2) return temp - 1;
            return 3 - temp;
        }

        private double getNoteFrequency(int note)
        {
            return 0.00390625 * Math.Pow(1.059463094, note - 128);
        }

    }
}
