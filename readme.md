# SonantSharp

SonantSharp is a reimplementation of [Sonant-X](https://github.com/nicolas-van/sonant-x), which in turn is a fork of [js-sonant](https://github.com/mbitsnbites/js-sonant), which in turn is a JS port of [Sonant](http://www.pouet.net/prod.php?which=53615) by the demoscene group Youth Uprising.

I recently stumbled across Sonant-X while reading a postmortem of a 13k JS game jam. My interest in synths and tiny code meant that I couldn't *not* mess around with this.

## Plans

I plan to support both the .json files used by Sonant-X and the .snt files created by the original Sonant.